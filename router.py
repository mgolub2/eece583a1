"""
EECE 583: Assignment One
Author: Maximilian Golub

Code for the router, using a modified lee more algorithm to
route to multiple sinks. The algorithm first routes a source
to a sink, then chooses from the remaining sinks until all sinks
are source connected. Connecting points for unconnected wire segments
are choosen by the closest point to a source connected wire.
"""

# Imports, all native python
import math
import time
from queue import PriorityQueue


class GridGraph(object):
    """
    The grid that the gui represents, used to preform the maze routing.
    Handles updating the gui when a node is added to a net.
    """

    ROUTE_DELAY = 0  # Change this to see more visible routing progress

    def __init__(self, cols, rows, gui, delay=ROUTE_DELAY):
        """
        Setup the GridGraph for a cols by rows grid, attached to a router gui.
        :param cols: The x width of the grid
        :param rows: The y width of the grid
        :param gui: The GUI to tie to this GridGraph
        :param delay: The routing delay between each node.
        """
        self.nodes = [GridNode]*cols
        self.height = rows
        self.width = cols
        self.sources = []
        self.sinks = {}
        self.sourced_wires = {}
        self.un_sourced_wires = {}
        self.gui = gui
        self.delay = delay
        # Init GridNodes for every location in the GridGraph
        for col in range(cols):
            self.nodes[col] = []
            for row in range(rows):
                self.nodes[col].append(GridNode(col, row, self))
        # Used to correctly size all boxes initially in the GUI.
        self.clean_nodes()

    def add_node_to_net(self, col, row, net, value=None, kind=None, fill=None, label=None, add=True):
        """
        Adds a node to a net based on col,row coordinates, optionally adding the node to the source
        or sinks sets.

        :param col: X coord of the node to add to a net
        :param row: Y coord of the node to add to a net
        :param net: The net to add the node to
        :param value: The path value of this node (How far away from the start of a path it is)
        :param kind: Kind can be a wire, source, sink, or obstruction.
        :param fill: The color to use for this node
        :param label: The text label for this node
        :param add: Add this node to the source or sink sets
        :return:
        """
        self.nodes[col][row].kind = kind
        self.nodes[col][row].value = value
        self.nodes[col][row].net = net
        if net is not None:  # If we just want to update the color, then the net can be none
            if net >= 0:
                if self.sourced_wires.get(net) is None:
                    self.sourced_wires[net] = set()
                if self.un_sourced_wires.get(net) is None:
                    self.un_sourced_wires[net] = set()
            if kind == "source" and add:
                self.sources.append(self.nodes[col][row])
                self.nodes[col][row].s_conn = True
            if kind == "sink" and add:
                if self.sinks.get(net) is None and net >= 0:
                    self.sinks[net] = set()
                self.sinks[net].add(self.nodes[col][row])
        if fill is None:
            try:
                self.nodes[col][row].fill = "#%02x%02x%02x" % (net * 75 % 255, net * 50 % 255, net * 150 % 255)
            except TypeError:  # If we don't have a net default to white
                self.nodes[col][row].fill = "white"
        else:
            self.nodes[col][row].fill = fill
        if label is None:
            # Label each node based on the net by default, and Sources with an S
            label = str(net) if kind != "source" else "S"
        # Preform the GUI update
        self.gui.color_rect(col, row, fill=self.nodes[col][row].fill, label=label)

    def clean_nodes(self):
        """
        Remove the values from all nodes on the GridGraph, and remove the
        active route flag.
        :return:
        """
        for col in self.nodes:
            for row in col:
                row.value = None
                row.active = False
                if row.kind is None:
                    self.add_node_to_net(row.col, row.row, None, fill="white", label="")

    def maze_route(self):
        """
        The maze router for the grid. Starts with the first source, uses basic algo,
        then selects a sink from the set of sinks to continue routing. Wire segments
        that are not source connected, wires that do not have a path to a source,
        are added back into the set of sinks by choosing the closest point in that net to
        a source connected wire. (Or source itself).
        :return:
        """
        # Stat tracking
        routed_nets = 0
        total_segments = 0
        for net in self.sinks.keys():
            total_segments += len(self.sinks[net])
        total_failed = 0
        failed_per_source = {}
        # Iterate over all the nets
        for source in self.sources:
            failed = 0
            attempts = 1  # We always attempt once
            print("Starting routing on {0}".format(source.net))
            source.value = 0
            self.route(source, PriorityQueue(), source)  # Route returns 1 if it couldn't find a route
            source.kind = "sink"  # Set the source to be a sink like everything else
            self.clean_nodes()  # Cleanup unused nodes
            while self.sinks[source.net]:  # While we have sinks still to route
                print("Routing {0} more sinks...".format(len(self.sinks[source.net])))
                sink = self.sinks[source.net].pop()  # Grab a sink
                sink.kind = "wire"  # Set it as a wire so we don't route right back to it.
                sink.value = 0
                # Try to route from the sink to another sink/wire
                failed += self.route(sink, PriorityQueue(), sink)
                if sink.s_conn != True:  # If the sink we just routed isn't connected to a source, try again
                    next_sink = self.search_closest(source)  # Find the closest point to route from
                    if next_sink is not None:
                        self.sinks[source.net].add(next_sink)  # Add it to set of sinks
                attempts += 1
                self.clean_nodes()
            print("Done routing net {0}".format(source.net))
            if failed == 0:
                routed_nets += 1
            failed_per_source[source] = failed/attempts*100
            total_failed += failed
        print("**************************************")
        print("Routed {:.2f}% of nets.".format(routed_nets/(len(self.sources))*100))
        total = 0
        for source in self.sources:
            total += failed_per_source[source]
            print("Failed to route {:.2f} percent of net {}."
                  .format(failed_per_source[source], source.net))
        print("Failed {:.2f}% of segments".format(total_failed/total_segments*100))
        print("Routed {0} segments.".format(total_segments-total_failed))

    def search_closest(self, source):
        """
        Calculate manhattan distance between a wires connected to a source for
        a given net and the closest unsourced wire.
        :param source: The source node to start from
        :return: The closest sink to the source
        """
        current_close_source = None
        #current_close_dest = GridNode
        current_distance = math.inf
        for node in self.sourced_wires[source.net]:
            for un_sourced in self.un_sourced_wires[source.net]:
                distance = abs(un_sourced.col-node.col)+abs(un_sourced.row-node.row)
                if distance < current_distance:
                    current_distance = distance
                    #current_close_dest = un_sourced
                    current_close_source = node
        return current_close_source

    def backup(self, node, start_node):
        """
        Preforms the backtracking component of the lee-more algorithm recursivly,
        choosing the lowest node that is par tof the net as the next node, stopping
        when node == start_node.
        :param node: The node to backtrack from
        :param start_node: The node which started the maze propagation, the stopping node
        :return:
        """
        back_node = node.lowest()
        node.s_conn = True if start_node.kind == "source" or start_node.s_conn else node.s_conn
        back_node.s_conn = node.s_conn
        if node.s_conn: # I if this node has a source
            self.sourced_wires[node.net].add(node)  # Add to sourced wires
            self.un_sourced_wires[node.net].discard(node)  # Remove from unsourced wires
        else:
            self.un_sourced_wires[node.net].add(node)
        if back_node is start_node:
            return 0
        back_node.kind = "wire"
        self.add_node_to_net(back_node.col, back_node.row, back_node.net,
                             label=None, kind=back_node.kind, value=back_node.value, add=False)
        return self.backup(back_node, start_node)

    def route(self, node, next_nodes, start_node):
        """
        The main part of the Lee More Algorithm. This recursive implementation
        goes through and finds all adjacent nodes to to node, placing them
        into a priority queue, next_nodes. The function is called recursively on the
        head of the next_nodes queue until a sink or non-active wire is found.
        :param node: The current node for neighbor discovery
        :param next_nodes: Priority queue of nodes
        :param start_node: The node which started the wave propagation
        :return:
        """
        time.sleep(self.delay)
        adj_nodes = node.get_adj_nodes()
        node.active = True
        for adj in adj_nodes:
            # We can make a connection if the next grid block is a sink or a wire we are not
            # Actively routing
            if (adj.kind == "sink" or adj.kind == "wire") and adj.net == node.net and not adj.active:
                if not (start_node.s_conn and adj.s_conn):
                    adj.value = node.value + 1
                    self.add_node_to_net(adj.col, adj.row, adj.net, label=None, value=adj.value,
                                         kind="wire", add=False)
                    self.sinks[adj.net].discard(adj)
                    return self.backup(adj, start_node)  # We've found a sink/wire!
            if adj.value is None and adj.net is None:
                value = node.value+1
                self.add_node_to_net(adj.col, adj.row, node.net, value=value, label=value)
                next_nodes.put(adj)
        if next_nodes.empty():  # We've failed to find a sink or wire to link to
            return 1
        return self.route(next_nodes.get(), next_nodes, start_node)


class GridNode(object):
    """
    Class for holding information about each node on the grid,
    includes adjacent nodes, the current value of the node, wether this
    node is in the active routing chain, etc.
    """

    def __init__(self, col, row, parent_grid, kind=None):
        """
        Creates a new node.
        :param col: X coord of node on grid
        :param row: Y coord of node on grid
        :param parent_grid: The parent grid this node is attached to
        :param kind: The type of node this is - wire, obstruction, source, sink
        """
        self.parent_grid = parent_grid
        self.col = col
        self.row = row
        # Positions of other nodes around this node - used for routing
        self.left = None if col == 0 else col-1
        self.right = None if col == parent_grid.width-1 else col+1
        self.up = None if row == 0 else row-1
        self.down = None if row == parent_grid.height-1 else row+1
        self.net = None  # Net to which this node is attached
        self.value = None  # Distance value of this node in a specific net
        self.kind = kind  # Type of node this is
        self.s_conn = False  # True if connected to a source
        self.active = False  # True if in current routing chain.

    def lowest(self):
        """
        Return the lowest valued node attached to this node that has a matching net
        :return: Lowest neighbor node in the same net as this node.
        """
        nodes = self.get_adj_nodes()
        lowest_node = math.inf
        for node in nodes:
            if node.value is not None:
                if node.value < lowest_node and node.net == self.net:
                    lowest_node = node
        return lowest_node

    def get_adj_nodes(self):
        """
        Returns a list of all valid adjacent nodes.
        :return: List of adjacent nodes.
        """
        not_none_nodes = []
        if self.left is not None:
            not_none_nodes.append(self.parent_grid.nodes[self.left][self.row])
        if self.right is not None:
            not_none_nodes.append(self.parent_grid.nodes[self.right][self.row])
        if self.up is not None:
            not_none_nodes.append(self.parent_grid.nodes[self.col][self.up])
        if self.down is not None:
            not_none_nodes.append(self.parent_grid.nodes[self.col][self.down])
        return not_none_nodes

    # Functions below here are for comparision/hashing a GridNode and don't matter to the algorithm.
    def __lt__(self, other):
        return self.value < other

    def __gt__(self, other):
        return self.value > other

    def __eq__(self, other):
        return self.value == other

    def __le__(self, other):
        return self.value <= other

    def __ge__(self, other):
        return self.value >= other

    def __hash__(self):
        """
        Insures uniqueness when a node is inserted into sets/dicts
        :return:
        """
        return hash(repr(self))