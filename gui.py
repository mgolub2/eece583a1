#!/usr/bin/python3
"""
EECE 583: Assignment One
Author: Maximilian Golub

Tested only on Python 3.6, any Python 3 should be fine.

Displays step by step the progress of the router code.
Generates an initial col x row grid, and parses the .infiles
to load the information into a router.GridGraph when run from the command line

Basic usage:
python3 gui.py <benchmark_file>

Options:
-d      Optional delay parameter, in seconds
-s      Scale in pixels of each box - useful for really large or small grids.
"""

try:  # Attempt to work with either python2 or python3.
    import Tkinter as tk
except ModuleNotFoundError:
    import tkinter as tk
import argparse
import router
import math
import threading
import sys

sys.setrecursionlimit(2500)  # stdcell recurses too much

class App(tk.Frame):
    """
    The gui for the router
    """
    # Constants to deal with the gui
    SCALE = 20  # Pixel size of each grid node
    BOUND = 1  # Boundary between the rectangles representing pins and objects and grid lines
    OFFSET = 3  # This is to offset everything from the canvas so the outer lines are drawn

    def __init__(self, rows, cols, master=None, scale=SCALE):
        """
        Sets up the initial GUI for the router.
        :param rows: Number of rows in the grid
        :param cols: Number of cols in the grid
        :param master:
        :param scale: Size of each rectangle
        """
        tk.Frame.__init__(self, master)
        #self.grid()
        self.rows = rows
        self.cols = cols
        self.scale = scale
        self.width = self.cols * self.scale+App.OFFSET
        self.height = self.rows * self.scale+App.OFFSET
        self.canvas = tk.Canvas(self.master, width=self.width, height=self.height)
        self.canvas.pack()
        for c in range(self.cols):  # Creates initial graph lines for the grid
            self.canvas.create_line(c * self.scale+App.OFFSET, App.OFFSET, c * self.scale+App.OFFSET, self.height)
            for r in range(self.rows):
                self.canvas.create_line(App.OFFSET, r*self.scale+App.OFFSET, self.width, r*self.scale+App.OFFSET)
        self.canvas.create_rectangle(App.OFFSET, App.OFFSET, self.width, self.height)  # Bounding rectangle

    def color_rect(self, col, row, fill, label=""):
        """
        Colors a rectangle of the grid based on xy coords. Insures lines aren't overdrawn.
        :param col: X coord of rectangle
        :param row: Y coord of rectangle
        :param fill: Color for rectangle
        :param label: Text for rectangle
        :return:
        """
        self.canvas.create_rectangle(col*self.scale+App.BOUND+App.OFFSET,
                                     row*self.scale+App.BOUND+App.OFFSET,
                                     (col+1)*self.scale-App.BOUND+App.OFFSET,
                                     (row+1)*self.scale-App.BOUND+App.OFFSET,
                                     fill=fill)
        self.canvas.create_text(col*self.scale+.6*self.scale, row*self.scale+.6*self.scale, text=label)


def parse_and_run(args):
    """
    Parses a .infile and creates a GridGraph, then loads obstructions,
    sources, and sinks into the GridGraph. Finally, a routing thread is started
    to attempt to maze_route all the sources to sinks.
    :param args: Command line arguments
    :return:
    """
    with open(args.netlist, 'r') as netlist_file:
        netlist = netlist_file.readlines()
    cols, rows = netlist[0].split()
    app = App(int(rows), int(cols), None, scale=args.scale)
    num_obstructions = int(netlist[1])
    grid = router.GridGraph(int(cols), int(rows), app, delay=args.delay)
    # Add obstructions to GridGraph
    for obstruction in range(1, num_obstructions+1):
        col, row = netlist[1+obstruction].split()
        grid.add_node_to_net(int(col), int(row), -1, kind="obstruction", value=math.inf, fill="blue", label="")
    wire_offset = int(netlist[num_obstructions+2])  # Number of nets
    net = 1
    # Add all sources and sinks to GridGraph
    for wire in range(num_obstructions+3, num_obstructions+3+wire_offset):
        wire_data = netlist[wire].split()
        num_pins = int(wire_data[0])
        for pin in range(0, num_pins*2, 2):
            col = int(wire_data[pin+1])
            row = int(wire_data[pin+2])
            if pin == 0:
                grid.add_node_to_net(col, row, net, value=0, kind="source")
            else:
                grid.add_node_to_net(col, row, net, kind="sink")
        net += 1
    app.master.title('Router')
    route_thread = threading.Thread(target=grid.maze_route, daemon=True)
    route_thread.start()  # Begin routing!
    app.mainloop()  # Draw the GUI on screen

# Runs the the parser for .infiles after gathering arguments
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("netlist", help="The netlist file to parse")
    parser.add_argument("-d", "--delay", help="Optional delay parameter, in seconds", default=0, type=int)
    parser.add_argument("-s", "--scale", help="Scale in pixels of each box -"
                                              " useful for really large or small grids.", default=20, type=int)
    parse_and_run(parser.parse_args())
