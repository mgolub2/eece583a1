"""
EECE 583: Assignment One
Author: Maximilian Golub

Unit tests for router.py, insuring most methods work as expected
Run: python3 test.py
"""

import unittest
import router  # Unit under test
import gui
from queue import PriorityQueue

class TestGridNodeMethods(unittest.TestCase):
    """
    Tests for the router.GridNode
    """

    def setUp(self):
        """
        Preforms a setup before each test creating the components to test
        :return:
        """
        self.gui = gui.App(5, 5)
        self.pg = router.GridGraph(5, 5, self.gui)
        self.pg.add_node_to_net(2, 2, 1, value=2)
        self.pg.add_node_to_net(1, 2, 1, value=1)
        self.pg.add_node_to_net(2, 1, 1, value=3)
        self.pg.add_node_to_net(3, 2, 1, value=10)
        self.pg.add_node_to_net(2, 3, 1, value=5)

    def test_lowest(self):
        """
        Tests the GridNode.lowest method
        :return:
        """
        self.assertTrue(self.pg.nodes[2][2].lowest() == self.pg.nodes[1][2])

    def test_get_adj_nodes(self):
        """
        Tests the GridNode.get_adj_nodes method
        :return:
        """
        self.assertTrue(len(self.pg.nodes[2][2].get_adj_nodes()) == 4)

    def test_lt(self):
        """
        tests less than method
        :return:
        """
        self.assertTrue(self.pg.nodes[2][2] < self.pg.nodes[3][2])

    def test_gt(self):
        """
        Tests greater than method
        :return:
        """
        self.assertTrue(self.pg.nodes[2][2] > self.pg.nodes[1][2])


class TestGridGraphMethod(unittest.TestCase):
    """
    Test methods in the router.GridGraph class
    """

    def setUp(self):
        """
        Preforms a setup before each test creating the components to test
        :return:
        """
        self.gui = gui.App(5, 5)
        self.pg = router.GridGraph(5, 5, self.gui)

    def test_route(self):
        """
        Test the route method, including backup
        :return:
        """
        self.pg.add_node_to_net(2, 2, 1, kind="source", value=0)
        self.pg.add_node_to_net(4, 4, 1, kind="sink")
        pq = PriorityQueue()
        self.assertTrue(self.pg.route(self.pg.nodes[2][2], pq, self.pg.nodes[2][2]) == 0)

    def test_backup(self):
        """
        Tests the backup method
        :return:
        """
        self.pg.add_node_to_net(2, 2, 1, kind="source", value=0)
        self.pg.add_node_to_net(3, 2, 1, value=1)
        self.pg.add_node_to_net(4, 2, 1, value=2)
        self.pg.add_node_to_net(4, 3, 1, value=3)
        self.pg.add_node_to_net(4, 4, 1, kind="sink", value=4)
        self.assertTrue(self.pg.backup(self.pg.nodes[4][4], self.pg.nodes[2][2]) == 0)

    def test_search_closest(self):
        """
        Tests the search closest method
        :return:
        """
        self.pg.add_node_to_net(2, 2, 1, kind="source", value=0)
        self.pg.add_node_to_net(4, 4, 1, kind="sink", value=4)
        self.pg.sourced_wires[1] = {self.pg.nodes[2][2]}
        self.pg.un_sourced_wires[1] = {self.pg.nodes[4][4]}
        self.assertTrue(self.pg.search_closest(self.pg.nodes[4][4]) == self.pg.nodes[2][2])

if __name__ == '__main__':
    unittest.main()